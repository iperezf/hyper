/**
 * Created by Iván on 20/01/2017.
 */

import { Component } from '@angular/core';

import { NavController,NavParams,LoadingController } from 'ionic-angular';
import { EventosService } from "../../services/eventos.service";


@Component({
  selector: 'page-evento-detalle',
  templateUrl: 'evento-detalle.html',
  providers:[EventosService]
})
export class EventoDetalle {
  public detalles;
  public errorMessage;

  constructor(public navCtrl: NavController,public params:NavParams, private _eventosService: EventosService, public loadingCtrl: LoadingController) {
    //alert(params.get("idEvento"));
    let idEvento=params.get("idEvento");

    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present(); //Muestro gif de carga.
    this._eventosService.getDetallesEvento(idEvento)
      .subscribe(
        result => {
          loader.dismiss(); //Aqui ya tengo los datos, puedo eliminar el gif de carga.
          this.detalles = result;
          console.log(this.detalles);
          //console.log(result[0].id);
          //console.log(this.eventos[0].titulo);
        },
        error => {
          this.errorMessage = <any>error;

          if(this.errorMessage !== null){
            console.log(this.errorMessage);
            console.log("Error en la peticion");
          }
        }
      );
  }
}
