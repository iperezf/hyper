import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { EventoDetalle } from '../evento-detalle/evento-detalle';
import { Perfil } from '../perfil/perfil';
import { Contacto } from '../contacto/contacto';
import { EventosMarcados } from "../eventos-marcados/eventos-marcados";
import { EventosService } from "../../services/eventos.service";

//import { Ayuda } from '../ayuda/ayuda';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[EventosService]
})
export class HomePage {
  public eventos;
  public errorMessage;
  perfilpage=Perfil;
  contactopage=Contacto;
  //ayudapage=Ayuda;
  eventosmarcadospage=EventosMarcados;


  constructor(public navCtrl: NavController, private _eventosService: EventosService) {
    this._eventosService.getEventos()
      .subscribe(
        result => {
            this.eventos = result;
            console.log(this.eventos);
            //console.log(result[0].id);
            //console.log(this.eventos[0].titulo);
        },
        error => {
          this.errorMessage = <any>error;

          if(this.errorMessage !== null){
            console.log(this.errorMessage);
            console.log("Error en la peticion");
          }
        }
      );
  }

  verEventoDetalle(id) {
    this.navCtrl.push(EventoDetalle, {
      idEvento: id
    });
  }

}
