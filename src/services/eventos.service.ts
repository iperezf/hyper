/**
 * Created by Iván on 31/01/2017.
 */


// importamos el modulo Injectable de AngularJS
import { Injectable } from '@angular/core';

import {Http, Response, Headers} from "@angular/http";
import "rxjs/add/operator/map";
import {Observable} from "rxjs/Observable";

// Permitimos que este objeto se pueda inyectar con la DI
@Injectable()
export class EventosService {
  constructor(private _http: Http){}
  // Definimos un método que recibe un parámetro y devuelve un string
  getEventos() {
    return this._http.get("https://hyper-moskitokemado.c9users.io/index.php?rutina=getEventos").map(res => res.json());
  }

  getDetallesEvento(id){
    return this._http.get("https://hyper-moskitokemado.c9users.io/index.php?rutina=getDetallesEvento&id="+id).map(res => res.json());
  }

}
