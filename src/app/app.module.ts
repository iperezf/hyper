import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { EventoDetalle } from '../pages/evento-detalle/evento-detalle';
import { Perfil } from '../pages/perfil/perfil';
import { Contacto } from '../pages/contacto/contacto';

import { EventosMarcados } from '../pages/eventos-marcados/eventos-marcados';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    EventoDetalle,
    Perfil,
    Contacto,
    EventosMarcados
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    EventoDetalle,
    Perfil,
    Contacto,
    EventosMarcados
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
